/*
Consider the following table of data types and corresponding bytes in memory.
Data Type Size
char 1 byte
int 4 bytes
float 4 bytes
double 8 bytes

i. Which data type would you use to store the number of undergraduate students in the
School of Computing Science? [1]

I would like to select 'int' data type. The reason being that the number of students is a whole number 
and is unlikely to exceed the maximum value that an int can hold (which is about 2 billion for a 32-bit int).

ii. Which data type would you use to store the value of pi (pi) to 10 decimal places?
How many bytes in memory would it require?
Give an example variable declaration. There is no need for the value to be correct.

To store the value of π(pi) to 10 decimal places, my suggestion is that use the "double" data type. 
This type is often used for floating-point numbers that require more precision, 
and it can comfortably handle the precision you're asking for. 
Since double data types take up 8 bytes in memory, storing π to 10 decimal places would require 8 bytes.
And the following is the example
*/
//example
char grade = 'A';               // A character
int num_students = 450;         // An integer
float avg_grade = 88.5f;        // A floating-point number
double pi = 3.1415926536;       // A double precision floating-point number


/*
(b) How many bytes of memory does the string literal “Systems” require in a C program? Show your working.

In C, string literals are implemented as arrays of characters. So, the size in memory of the string literal "Systems"
can be determined by the number of characters in the string, including the null character ('\0') that's automatically a
dded at the end of the string by the C compiler.
So, the total memory required to store the string literal "Systems" in a C program is 7 bytes
(for the characters 'S', 'y', 's', 't', 'e', 'm', 's') plus 1 byte (for the null character), which equals 8 bytes.
*/

/*
(c)Consider the following struct definition and use.
How many bytes of memory in total will the data stored in each of the guy and brad variables occupy?
Explain how you calculated your answers.
*/

struct actor { 
	char* name;
	int age; 
	char* famous_for; 
}; 

struct actor guy = {"Guy Pearce", 55,"Memento"}; 
struct actor brad = {"Brad Pitt", 58, "Seven"};

/*
The strings "Guy Pearce", "Memento", "Brad Pitt", and "Seven" will occupy 10 bytes, 8 bytes, 10 bytes, and 6 bytes respectively (including the null terminating character).

So, for guy, the total memory occupied is 20 bytes (for the struct) + 10 bytes (for "Guy Pearce") + 8 bytes (for "Memento") = 38 bytes.
And for brad, the total memory occupied is 20 bytes (for the struct) + 10 bytes (for "Brad Pitt") + 6 bytes (for "Seven") = 36 bytes.
*/

/*fill the has_ch function, and blew is the full code*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF 256

int has_ch(char ch, char *line) {
    return strchr(line, ch) != NULL;
}

int main(int argc, char *argv[]) {
    FILE *fp;
    char ch;
    char line[BUF];

    if (argc != 3) {
        printf("Usage: %s character filename\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    ch = argv[1][0];
    
    if ((fp = fopen(argv[2], "r")) == NULL) {
        printf("Error: Cannot open %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    while (fgets(line, BUF, fp) != NULL) {
        if (has_ch(ch, line)) {
            fputs(line, stdout);
        }
    }
    
    fclose(fp);
    return 0;
}

#include <stack>
#include <mutex>
#include <condition_variable>
#include <iostream>

class ThreadSafeStack {
private:
    std::stack<int> stack;
    std::mutex mtx;
    std::condition_variable cv;
    const size_t maxSize;

public:
    ThreadSafeStack(size_t maxSize): maxSize(maxSize) {}

    void push(int val) {
        std::unique_lock<std::mutex> lock(mtx);
        cv.wait(lock, [this] { return stack.size() < maxSize; });
        stack.push(val);
        cv.notify_all();
    }

    int pop() {
        std::unique_lock<std::mutex> lock(mtx);
        cv.wait(lock, [this] { return !stack.empty(); });
        int val = stack.top();
        stack.pop();
        cv.notify_all();
        return val;
    }

    bool try_push(int val) {
        std::lock_guard<std::mutex> lock(mtx);
        if (stack.size() < maxSize) {
            stack.push(val);
            cv.notify_all();
            return true;
        }
        else {
            return false;
        }
    }

    bool try_pop(int &val) {
        std::lock_guard<std::mutex> lock(mtx);
        if (!stack.empty()) {
            val = stack.top();
            stack.pop();
            cv.notify_all();
            return true;
        }
        else {
            return false;
        }
    }
};



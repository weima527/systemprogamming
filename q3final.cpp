pthread_mutex_t m;
	bool delivered = false;
	void *homeOwner(void* arg)
	{
		printf("HomeOwner: Awaiting delivery ...\n");

	pthread_mutex_lock(&m);
	while (!delivered) {
	pthread_mutex_unlock(&m);
	printf("HomeOwner: Still waiting\n");
	pthread_mutex_lock(&m);
	}
	pthread_mutex_unlock(&m);

	printf("HomeOwner: Enjoying my new item.\n");
	return NULL;
	}
	void *deliveryDriver(void* arg)
	{
	printf("Delivery Driver: out making deliveries ...\n");

	usleep(randomSleepTime());
	printf("Delivery Driver: arrived at address\n");

	pthread_mutex_lock(&m);
	delivered = true;
	pthread_mutex_unlock(&m);

	printf("Delivery Driver: package delivered\n");
	return NULL;
	}
	int main()
	{
	/* Perform some initialisation */
	pthread_t t1;
	pthread_t t2;
	pthread_mutex_init(&m, NULL);
	pthread_create(&t1, NULL, homeOwner, NULL);
	pthread_create(&t2, NULL, deliveryDriver, NULL);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	pthread_mutex_destroy(&m);
	}

/*
Use the correct technical terms to give brief and precise answers to the following questions about the properties of
the program. In each case justify your answer.
(a) Is the program parallel or concurrent, as defined in the course?

The program fragment is concurrent, as it contains two threads that can execute simultaneously

(b) Identify the state shared by the two threads.

The state shared by the two threads homeOwner and deliveryDriver is the boolean variable 'delivered'.

(c) Use the program line numbers to identify any critical section(s) in the program fragment.

In the 'homeOwner' function, is line 6-12; In the 'deliveryDriver' function is line 21-23

(d) Is the program fragment thread safe? If not, carefully explain how an inconsistent state may arise. If so, carefully
explain how this is achieved.

The given program fragment is thread-safe. by using a mutex (m) to protect the shared variable delivered. 
The mutex ensures that only one thread can access, and it preventing inconsistent states and making the program thread-safe.

(e)
(i) Briefly outline any other concurrency issues you identify with the program fragment.

One concurrency issue in the program is busy waiting or spin-waiting, which occurs in the homeOwner function.

(ii) Explain the implications of the concurrency issue(s) you identify.

Busy waiting is when a thread keeps checking a condition repeatedly instead of waiting when it can't proceed. 
In the 'homeOwner' function, this happens when the delivery hasn't arrived yet. 
Assume it can using a condition variable, itwould be a better approach,
as it allows the thread to sleep until the condition is met, instead of wasting CPU time.

(iii) Explain how the program can be adapted to avoid the concurrency issue(s) you have identified, and explain how
this addresses the implications you have identified.

(iv) Adapt the program fragment to avoid the concurrency issue(s) you have identified. Write out the complete
program fragment, and provide comments explaining the purpose of ea*/

pthread_mutex_t m;
pthread_cond_t delivered_conditon; // Condition variable
bool delivered = false;

void *homeOwner(void* arg) {
    ...

    pthread_mutex_lock(&m);
    while (!delivered) {
        pthread_cond_wait(&delivered_conditon, &m); // Wait for the condition variable
    }
    pthread_mutex_unlock(&m);

    ...
}

void *deliveryDriver(void* arg) {
    ...

    pthread_mutex_lock(&m);
    delivered = true;
    pthread_cond_signal(&delivered_conditon); // Signal the condition variable
    pthread_mutex_unlock(&m);

    ...
}

int main() {
    ...

    pthread_mutex_init(&m, NULL);
    pthread_cond_init(&delivered_conditon, NULL); // Initialize condition variable

    ...

    pthread_mutex_destroy(&m);
    pthread_cond_destroy(&delivered_conditon); // Destroy condition variable

}

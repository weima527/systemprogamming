/*
Q2 Memory and Resource Management & Ownership 
(a) Consider the following allocation: 
	foo * × = malloc(sizeof(foo));
	Where is x stored? 
	Where is the value it points to stored? 


`x` is a pointer, and its own value stored on the stack
The memory that `x` points to, allocated with `malloc`, is stored on the heap.
When you call `malloc`, it reserves a block of memory of the requested size somewhere on the heap, 
and then returns a pointer to the start of that block.
So the value that `x` points to (the `foo` object) is on the heap. 

Thus, 'x' is stored on the stack. The value 'x' points to is stored on the heap.

/*
Consider the following declaration: 
int matrix[40][20]; 
Express the address of the first element in the 20th row in two different ways.

The arrays are zero-indexed, so the "20th row" would actually be accessed with the index 19 
(since the first row is the 0th row, the second row is the 1st row).

The address of the first element in the 20th row can be represented in these two ways:

1. Using array notation: `&matrix[19][0]`
2. Using pointer notation: `*(matrix + 19)`

Both of these will provide the memory address of the first element in the 20th row of the array. 
*/

/*
Declare a function called sub str that returns a pointer to a string, 
and takes as arguments a string and an integer. 
There is no need to define the logic of the function []

*/

char* sub_str(char* str, int num);
//This function takes a string `str` and an integer `index` as arguments.
// The `index` parameter specifies the starting index of the substring to be returned. The function returns a pointer to the substring.

/*
Consider the following code that traverses a block of memory pointed to by buf. */
// the traverse function 
int traverse(char buf[], int n){ 
	int ok = 1; 
	int i; 
	for (i = 0; i<=n-1; i++) {
	 if (buf[i]!=buf[n-1-i]) 
	 	ok = 0; return ok;
}
//Explain in your own words the aim of this code. Explain how this aim is achieved. 
//Give one example of an input buf that would mean the function returns the value 1.  

/*
The `traverse` function aimed to check whether the given character array `buf` of length `n` is a palindrome. 
for example, if it reads the same forwards and backwards. 

The function initializes an variable `ok` to 1, which assumes that the block is a palindrome until proven otherwise. 
Then, it enters a loop that checks each element in `buf` from the beginning against the corresponding element from the end. 
If it finds a pair that doesn't match, then it sets `ok` to 0 (false), which indicated that the block is not a palindrome. 
Finally, exits the loop.

For example, if the input buf is "racecar", the function would return 1 because the block is a palindrome.
The for loop would compare the first and last characters ('r' and 'r'), 
then the second and second-to-last characters ('a' and 'a'),
and finally the third and third-to-last characters ('c' and 'c'), finding them all to be equal. 
Therefore, ok remains 1 and the function returns 1.
*/

/*
e) The program fragments below contain some error(s). 
For each fragment, name the type of error(s) and give a brief technical explanation of how it arises. */

 node * some_node = create node(...); 
 node * other_node = create node(..., some_node, ...); 
 free(some_node); 

/* ErrorType: "dangling pointer" error

The code creates two nodes using the `create_node` function. 
The second node has a reference to the first node as one of its parameters. 
Then, the first node is freed using the `free` function. 
This means that the memory allocated for the first node is released and can be used for other purposes.
However, the second node still has a reference to the first node, which is now invalid since the memory has been freed. 
This can lead to undefined behavior when the second node tries to access the freed memory. 

To avoid this error, the second node should not have a reference to the first node, or the first node should not be freed until all references to it have been removed.
*/

 int s = 3; 
 void * p = malloc(sizeof(int)*s); 
 void * q= p; 
 free(p);
 free(q); 

 /*Error type:  "double free" error. 

When the memory is allocated using malloc, a block of memory of size `sizeof(int)*s` is allocated and a pointer `p` is assigned to the starting address of the block.
Then, the pointer `q` is assigned the same address as `p`. 

When `free(p)` is called, the memory block is deallocated and the pointer `p` becomes a dangling pointer, pointing to an invalid memory location. 
When `free(q)` is called, it tries to deallocate the same memory block that has already been deallocated by `free(p)`.
This results in undefined behavior, which in this case is a double free error.

To fix this error, we should only call `free` once for each block of memory that has been allocated using `malloc`. 
And we should also avoid using dangling pointers,. */

int s = 3;
void *p = malloc(sizeof(int) * s);
if (p != NULL) {
    // use the memory block
    free(p);
    p = NULL;
}
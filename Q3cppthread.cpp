/*3.	Concurrent Systems Programming

(a)	Explain the difference between Concurrency and Parallelism, as discussed in the Lecture.

Concurrency is a programming paradigm; Parallelism is about making programs faster
Concurrency is about dealing with lots of things at once; Parallelism is about doing lots of things at once
(b)	Describe briefly one mechanism that is used to protect critical sections in multi-threaded code. 

Mutual exclusion which is e.g. ensured via mutexes (but possibly also via other related mechanisms such as semaphores)

(c)	Look at the multi-threaded source code sketch below.
*/

int main() {
  std::stack<Stuff> buffer;
  buffer.push(make_stuff());
  buffer.push(make_stuff());

  auto t1 = std::thread([bufferPtr = &buffer]() {
    for (int i = 0; i < 1000; i++)
      bufferPtr->push(make_stuff());
  });

  auto t2 = std::thread([bufferPtr = &buffer]() {
    for (int i = 0; i < 1000; i++)
      bufferPtr->pop();
  });

  t1.join(); t2.join();
  printf(“%d\n”, buffer.size());
}


/*
i) Is this program safe? If not explain why.
ii) What will be the size of the buffer at the end of the program?
Explain your answer.

No this program is not safe, as the buffer is unprotected, but two threads simultaneously access it. (2 marks for this answer) 
It is possible that bufferPtr->pop can be called on an empty buffer.
As this program has a race condition, we cannot reasonable expect any single answer.
This program might crash or result in a buffer with (almost) arbitrary size.
*/

/*
(d)	A counting semaphore allows up to N many concurrent accesses to a resource.

As discussed in the lecture, a semaphore is created with an initial value create(int N) where N > 0. 
It has two atomic operations (wait and signal) which do logically the following:
*/

void wait(semaphore S) {
  while (S <= 0)
    ; // do nothing
  S--;
}

void signal(semaphore S) {
  S++;
}

/*
Provide an implementation for a counting semaphore that uses condition variables and no busy waiting. 
i) Provide the implementation of a struct semaphore	
ii) Provide a function void sem_wait(struct semaphore*) and 	
iii) a function void signal(struct semaphore*) 	
You answer has to be thread safe.
You can either use C PThreads or C++ threads in your answer.
You do not have to provide any initialisation code.
*/

  
//Or an equivalent implementation with C++ Threads
//4 marks for a definition of a semaphore struct.
//4 marks for sem_signal (1 for lock/unlock, 1 for right condition, 1 for count, 1 signal)
//4 marks for sem_wait (1 for lock/unlock, 1 for right while, 1 for wait, 1 for signal) sem_create not necessary

/*
The question is about the mutex exclusion part
Q1: What happens if some control flow fails to unlock a mutex?
Q2: What happens if some control flow fails to lock a mutex? 

A: If a control flow fails to unlock a mutex, it typically leads to a state known as a "deadlock". 
This occurs when a thread has locked a mutex but fails to unlock it before it exits. 
As a result, other threads that attempt to lock the mutex will block indefinitely, waiting for a signal that the mutex has been unlocked. 
If the thread that originally locked the mutex has exited without unlocking it, this signal will never come. 
Consequently, the threads waiting for the mutex to be unlocked will be stuck in a "blocked" state indefinitely, leading to a deadlock.

A: If a control flow fails to lock a mutex before accessing a shared resource, it may lead to a race condition.
	A race condition occurs when the behavior of a program depends on the relative timing of threads. 
	Without a lock, two or more threads can access and modify shared data simultaneously, leading to inconsistent or unexpected results. 
	In some cases, failing to lock a mutex can also lead to undefined behavior, which can be difficult to diagnose and debug. 

Therefore, it is crucial to correctly manage mutex locks in multi-threaded programming to ensure both the correctness and safety of your program.
 Always ensure that every lock operation has a corresponding unlock operation and that shared resources are adequately protected by locks.*/

//1. C programming and data types

/*(a)	In the lectures we described the purpose of data types with the phrase
“data types give bits meaning”.
Explain this phrase briefly by explaining how data types help to write meaningful programs and prevent errors. Give an example to support your answer.

A: Data types give bits meaning by providing structure and constraints to the raw data stored in memory. 
They help programmers write meaningful programs and prevent errors by enforcing rules on data operations and ensuring proper memory usage. 
For example, using an integer data type for an "age" variable restricts it to hold only whole numbers, preventing errors when assigning non-integer values like "John" to the variable.
*/

/*
(b)	Implement a doubly-linked list in the C programming language.

Sketch the implementation based on the following interface. Minor syntax errors will not be penalised. 
Make sure that you handle pointers correctly. Provide comments explaining your implementation.

// a node in the list should be able to store a value of any type
typedef struct node Node;
// the list should store its left and right end
typedef struct list List; 
// create an empty list; returning NULL if unsuccessful
List* list_create();
// destroys the list and all remaining nodes in it
void list_destroy(List* l);
// allocate a new node and add it to the right side of the list
void push_right(List* l, void* value);
// remove the left most node from the list and return it
Node* pop_left(List* l);
// visit each node (from left to right) and call the function
// for each visited node. The function cannot be called with a
// NULL pointer.
void for_each_node(List* l, void(*fun)(Node*));


i) Define the structs. Node should be able to store a value of an arbitrary type. List should store both ends of the list (its left and right end). 	[4]
ii) Implement list_create and list_destroy.	[4]
iii) Implement push_right and pop_left.	[5]
iv) Implement for_each_node.	[2]
v) Implement a main function that uses the provided interface to create a list and push three nodes with string values on the list.
 The for_each_node function should be used to print all values before the list is destroyed. There should be no memory leaks.*/

#include <stdio.h>
#include <stdlib.h>

// Define the Node struct
typedef struct Node {
    void* value;             // Stores a value of an arbitrary type
    struct Node* prev;       // Pointer to the previous node
    struct Node* next;       // Pointer to the next node
} Node;

// Define the List struct
typedef struct List {
    Node* leftEnd;           // Pointer to the left end of the list
    Node* rightEnd;          // Pointer to the right end of the list
} List;

// Function to create a new list
List* list_create() {
    List* newList = (List*)malloc(sizeof(List));
    if (newList != NULL) {
        newList->leftEnd = NULL;newList->rightEnd = NULL;
    }
    return newList;
}

// Function to destroy the list and free memory
void list_destroy(List* list) {
    Node* current = list->leftEnd;
    while (current != NULL) {
        Node* next = current->next;
        free(current->value); free(current);
        current = next;
    }
    free(list);
}

// Function to push a node to the right end of the list
void push_right(List* list, void* value) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode != NULL) {
        newNode->value = value;
        newNode->prev = list->rightEnd;
        newNode->next = NULL;

        if (list->rightEnd != NULL) {
            list->rightEnd->next = newNode;
        } else {
            list->leftEnd = newNode;
        }

        list->rightEnd = newNode;
    }
}

// Function to remove and return the leftmost node from the list
Node* pop_left(List* list) {
    if (list->leftEnd == NULL) {
        return NULL;
    }

    Node* leftmost = list->leftEnd;
    list->leftEnd = leftmost->next;

    if (list->leftEnd != NULL) {
        list->leftEnd->prev = NULL;
    } else {
        list->rightEnd = NULL;
    }

    return leftmost;
}

// Function to iterate over each node in the list
void for_each_node(List* list, void (*fun)(Node*)) {
    Node* current = list->leftEnd;
    while (current != NULL) {
        fun(current);
        current = current->next;
    }
}

// Custom print function for for_each_node
void print_value(Node* node) {
    printf("%s\n", (char*)(node->value));
}

int main() {
    // Create a new list
    List* list = list_create();

    // Push three nodes with string values
    char* value1 = "Node 1";
    char* value2 = "Node 2";
    char* value3 = "Node 3";
    push_right(list, value1);
    push_right(list, value2);
    push_right(list, value3);

    // Print all values using for_each_node
    for_each_node(list, print_value);

    // Destroy the list and free memory
    list_destroy(list);

    return 0;
}
